# FAMILY LEVEL BAR PLOT BY TREATMENT, WITH PROPORTIONS OF EACH DIVISION
p<-ggplot(famcountdummy, aes(x=order,y=family_totals, fill = Division))+geom_bar(position="stack")+labs(y="Family Richness", x="Zone and Treatment")
p+scale_fill_manual(values=c("#009900", "#663300", "#CC0000")) + theme(axis.text.x=element_text(size=12)) + theme(axis.text.y=element_text(size=12))+
  theme(legend.text = element_text(size = 14), legend.title=element_text(size=14))+ theme(axis.title.x = element_text(size=14), axis.title.y=element_text(size=14))
# END OF FAMILY LEVEL BAR PLOT STUFF


# MULTIVARIATE ANALYSIS OF PLOTS BY LOW, MID AND HIGH ZONE - NONMETRIC MULTIDIMENSIONAL SCALING
library(MASS)
d<-dist(NMDS)
fit<-isoMDS(d, k=2)
fit<-data.frame(fit)
fit$zone<-installeleven$zone
lowfit<-subset(fit, zone=="low") 
lx<-lowfit$points.1
ly<-lowfit$points.2
midfit<-subset(fit, zone=="mid")
mx<-midfit$points.1
my<-midfti$points.2
highfit<-subset(fit, zone=="high")
hx<-highfit$points.1
hy<-highfit$points.2
# MAKE SUBSETS OF THE OTHER DATASETS AND MAKE VECTORS FOR THEM TOO
plot(c(-170, 180), c(-170,120), cex.lab = 1.25, cex.axis= 1.25, type="n", xlab= "Coordinate 1", ylab="Coordinate 2")
points(lx,ly, col="black", pch=15, cex=1.2)
points(mx,my, col="red", pch=19, cex=1.2)
points(hx,hy, col="blue", pch=17, cex=1.2)
legend("bottomright", c("Low", "Mid", "High"), pch=c(15, 19, 17), cex = 1.2, col=c("black", "red", "blue"))


# MULTIVARIATE ANALYSIS OF PLOTS BY ZONE AND TREATMENT - NONMETRIC MULTIDIMENSIONAL SCALING
lowsgafit<-subset(fit, zonetreat=="lowSGA")
lowsgrfit<-subset(fit, zonetreat=="lowSGR")
lowcgafit<-subset(fit, zonetreat=="lowCGA")

midsgafit<-subset(fit, zonetreat=="midSGA")
midsgrfit<-subset(fit, zonetreat=="midSGR")
midcgafit<-subset(fit, zonetreat=="midCGA")

highsgafit<-subset(fit, zonetreat=="highSGA")
highsgrfit<-subset(fit, zonetreat=="highSGR")
highcgafit<-subset(fit, zonetreat=="highCGA")

lsgax<-lowsgafit$points.1
lsgay<-lowsgafit$points.2
lsgrx<-lowsgrfit$points.1
lsgry<-lowsgrfit$points.2
lcgax<-lowcgafit$points.1
lcgay<-lowcgafit$points.2
msgax<-midsgafit$points.1
msgay<-midsgafit$points.2
msgrx<-midsgrfit$points.1
msgry<-midsgrfit$points.2
mcgax<-midcgafit$points.1
mcgay<-midcgafit$points.2
hsgrx<-highsgrfit$points.1
hsgry<-highsgrfit$points.2
hsgax<-highsgafit$points.1
hsgay<-highsgafit$points.2
hcgax<-highcgafit$points.1
hcgay<-highcgafit$points.2

plot(c(-170, 180), c(-170,120), type="n", cex.lab = 1.25, cex.axis= 1.25, xlab= "Coordinate 1", ylab="Coordinate 2")
points(lcgax, lcgay, col="black", pch=15, cex=1.25)
points(mcgax, mcgay, col="red", pch=15, cex=1.25)
points(hcgax, hcgay, col="blue", pch=15, cex=1.25)
points(lsgax, lsgay, col="black", pch=22, cex=1.25)
points(msgax, msgay, col="red", pch=22, cex=1.25)
points(hsgax, hsgay, col="blue", pch=22, cex=1.25)
points(lsgrx, lsgry, col="black", pch=7, cex=1.25)
points(msgrx, msgry, col="red", pch=7, cex=1.25)
points(hsgrx, hsgry, col="blue", pch=7, cex=1.25)

legend("bottomright", cex=.87, c("Low Control", "Low Succession Grazers +", "Low Succession Grazers -", "Mid Control", "Mid Succession Grazers +", "Mid Succession Grazers -", "High Control", "High Succession Grazers +", "High Succession Grazers -"), pch=c(15, 22, 7, 15, 22, 7, 15, 22, 7), col=c("black", "black", "black", "red", "red", "red", "blue", "blue", "blue"))