setwd("~/Documents/Algae/Data\ from\ Plots/3rd\ year")
install.packages("ape")
library(ape)
library(picante)
tree<-read.nexus("redtol.nex")
algae<-read.csv("2012_08_low_forr.csv", row.names=1)
algaewhole<-read.csv("2012_08_low_forr_whole.csv", row.names=1)
prunedtree<-prune.sample(algaewhole,tree)


# INSTEAD LET'S USE THIS NMDSFAMILY (or manfamplot) DATAFRAME FOR LOW, MID AND HIGH ZONES
phyloalgae<-meanfamplot[,c(1,2,4,5,6,10,11,12,13,15,16,20,21,22,24,28,29)]
colnames(phyloalgae)[7]<-"Delesseriaceae"
phyloalgaezone<-with(phyloalgae,aggregate(phyloalgae[,c(3:17)], by=list(zone=Z), mean))

tree<-read.nexus("redtol.nex")
prunedtree<-prune.sample(phyloalgae,tree)


#plot the tree
plot(prunedtree)
#make branch lengths for the tree
prunedtree<-compute.brlen(prunedtree, method = "Grafen", power = 1)

#phylogenetic distance
phydist <- cophenetic(prunedtree)
ses.mpd.result <- ses.mpd(algaewhole, phydist, null.model = "taxa.labels", abundance.weighted = TRUE, runs = 4000)
ses.mpd.result

#let's make a dendrogram to see which communities are similar to each other.
library(cluster)
commdist.result<-comdist(algaewhole, phydist)
commdist.clusters<-hclust(commdist.result)
plot(commdist.clusters)

#export a tree 
write.nexus(prunedtree, file = "prunedtree.nex")