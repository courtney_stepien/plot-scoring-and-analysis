#basic richness and diversity indices for the August 2012 census
setwd("~/Documents/Algae/Data\ from\ Plots/3rd\ year")
install.packages("vegan") #The diversity 
library(vegan)
library(MASS)
census<-read.csv("2012_08_census_forr.csv", row.names=1) #if you want to give the rows names, use row.names=column number containing the names
census[is.na(census)]<-0 #replace all na's with 0's
??vegan #loads the help page for the vegan package, with pdf documentation for diversity analysis

# First let's look at species composition - the most common 5 species and their percent cover for each treatment
# We'll do this in the 2011 dataset first, since it's just easier.
# I can compare the means of the control 2011 versus control 2011-2012 easily, to see if the rank changes wildly.
installeleven<-subset(census, Install==2011) # What did we learn? You can't put numbers in dataframe names
installeleven[1:5, 1:12] # a way to view part of the dataframe (until I figure out that shortcut from the R book)

#Let's get richness index for each plot
familybarplot<-familyfigure[,c(2,9:11,14:40,47:48)]

with(installeleven, aggregate(installeleven[,c(13:72)], by=list(Z=zone, Treat=treatment), mean)) #The sickest function I have ever made.  
#within a dataset called installeleven, give me the mean percent cover of each algae species by intertidal zone and by treatment category

means<-with(installeleven, aggregate(installeleven[,c(13:72)], by=list(Zone=zone, Treat=treatment), mean))
mean.sd<-with(installeleven, aggregate(installeleven[,c(13:72)], by=list(Zone=zone, Treat=treatment), sd)) # Standard deviation of the matrix
mean.se<-mean.sd 
mean.se[,3:62]<-mean.se[,3:62]/sqrt(6) # Get the standard error on the mean of each treatment/zone category

#how to make a table that i can graph as a sweet composition plot
ids<-means[,1:3] #just the zone and plot info, we will append the reordered values to this matrix later.

# Normalizing percent cover - because they don't currently take into account barnacles, mussels, large inverts...
norm<-100/means[,3] #make a vector that is 100/each % cover, this will be multiplied across the table
normmeans<-means[,3:62]*norm # New dataframe, normmeans, where I'll multiply norm 
normalized<-data.frame(means[,1:2], normmeans)

#### normalized = based on the orignal table of mean values, in which mean percent covers have been normalized to 100% for algae. 
#### means = the original table of mean values for each of the 9 distinct treatments
#### topspecies = the top x number of species contributing to percent cover in each treatment
installelevent<-t(installeleven)
which(installeleven[1,45:4]!=0)
with(installeleven[,13:72], installeleven[sum()])
installeleven[,sapply(installeleven, is.numeric)]
#below is getting the top 5 contributors to percent cover, and making the rest of the values 0 so I can bar plot it pretty
# To change the number of species, change the number 5 in the second if statement to another number. 
for (i in 1:9){ 
    ids<-normalized[i,1:3] 
    species<-normalized[i,4:62] 
    speciesorder<-species[,order(species, decreasing=TRUE)] 
    print(ids)
      for (j in 1:59){
        if (speciesorder[1,j]<speciesorder[1,5]) {
          speciesorder[1,j]<-0
        }
    }
    if (i<2) {
      top5species<-data.frame(ids,speciesorder)
    }
    else {
      nextrow<-data.frame(ids,speciesorder)
      top5species<-rbind(top5species, nextrow)
    }
}

# Now I want to take the above code and use it to develop something that prints the values that add up to 10 percent, from lowest to highest
# And then I will turn all those columns to 0, and at some point add a 10% "other" column. 

for (i in 1:9){ 
  ids<-normalized[i,1:3] 
  species<-normalized[i,4:62] 
  speciesorder<-species[,order(species, decreasing=TRUE)] 
  richness<-0
  zero<-0
  for (k in 1:59) {
    if (speciesorder[1,k]>0){
      richness<-richness+1
    } else {
      zero<-zero+1
    }
  }
  ids<-cbind(ids, richness, zero)
  for (j in 1:58){
    if (sum(speciesorder[1,(59-j):59])>15) {
      nother<-length(speciesorder[1,(59-j+1):59])-zero
      print(speciesorder[1,(59-j+1):59])
      print(sum(speciesorder[1,(59-j+1):59]))
      speciesorder[1,(59-j+1):59]<-0
      ids<-cbind(ids, nother)
      break
    }
  }
  if (i<2) {
      topspecies<-data.frame(ids,speciesorder)
  } else {
      nextrow<-data.frame(ids,speciesorder)
      topspecies<-rbind(topspecies, nextrow)
  }
}
Other<-c(14.69194,14.03509,12.9666,14.52381,13.27586,13.12217,11.6349,14.6029,14.44444)
other<-data.frame(Other)
topspecies<-data.frame(topspecies,Other)
# Getting species counts on each of the 9 unique treatments (I already have them by zone, so now by zone-treatment)
richness<-0
zero<-0
for (k in 1:59) {
  if (speciesorder[1,k]>0){
    richness<-richness+1
  } else {
    zero<-zero+1
  }
}
ids<-cbind(ids, richness, zero)
# Sweet, the above code gets me richness counts for each row of the means table

#Transposing for easy bar plot (could have used 'melt' as well but not familiar with that yet)
normalizedt<-normalized[,4:62] # Need to transpose the data frame to bar plot it. 
id<-c("highC", "lowC", "midC", "highG+", "lowG+", "midG+", "highG-", "lowG-", "midG-") # made a set of row names for the normalizedt
rownames(normalizedt)<-id #this way when we transpose it, the row names will become the new column names
normalizedt<-t(normalizedt) #transpose!
grazercompare<-normalizedt[,c(5,8,6,9,4,7)]
successioncompare<-normalizedt[,c(2,5,3,6,1,4)]
colnames(successioncompare)<-c("lowC", "lowS", "midC", "midS", "highC", "highS")
colnames(grazercompare)<-c("lowG+", "lowG-", "midG+", "midG-", "hiG+", "hiG-")

# Alternatively now, I can do the the same as above, but for topspecies
topspeciest<-topspecies[,6:65] # Need to transpose the data frame to bar plot it. 
id<-c("highC", "lowC", "midC", "highSG+", "lowSG+", "midSG+", "highSG-", "lowSG-", "midSG-") # made a set of row names for the normalizedt
rownames(topspeciest)<-id #this way when we transpose it, the row names will become the new column names
normalizedt<-t(topspeciest) #transpose!
grazercompare<-normalizedt[,c(5,8,6,9,4,7)]
successioncompare<-normalizedt[,c(2,5,3,6,1,4)]
colnames(successioncompare)<-c("lowC", "lowS", "midC", "midS", "highC", "highS")
colnames(grazercompare)<-c("lowG+", "lowG-", "midG+", "midG-", "hiG+", "hiG-")

# to pick only columns that are not equal to zero....
rowSums(normalizedt>0) #this gives the sum of the number of times in each row a value is greater than 0 for each row
normalizedt[(rowSums(normalizedt>0)>0),] # return only the sums that are greater than 0
normalizedt[rowSums(normalizedt)>0,] # This line does the exact same thing, but cuts out the instances
rowSums(normalizedt)>0 # This by itself gives a list of TRUE/FALSE to show which rows satisfy the condition you described. 
normalizedtnonzero<-normalizedt[rowSums(normalizedt)>0,]
successioncompare<-successioncompare[rowSums(successioncompare)>0,]
grazercompare<-grazercompare[rowSums(grazercompare)>0,]

#Bar plots!
# Species composition
barplot(normalizedt[,4:9], main="Successional plots grazer versus no grazer", col=rainbow(60))
barplot(grazercompare, main="Successional plots grazer versus no grazer", col=rainbow(29))
barplot(successioncompare, main="Control versus successional communities", col=rainbow(26))#col=gray(seq(0,1, length=26)))  #legend = rownames(successioncompare))
barplot(successioncompare, angle=45+90*0:1, col="black", density=c(1,2,4,6,8,10,12,14,16,18,20,22,24), add=TRUE) #angle=0+30*0:12 to change angel of lines
barplot(successioncompare, angle=45+90*1:2, col="black", density=c(0,1,4,6,8,10,12,14,16,18,20,22,24), add=TRUE)
par(xpd=TRUE)
text(c(.75, 2, 3.2, 4.25, 5.5, 6.75), 102, c("25(13)", "28(16)", "24(18)", "26(18)", "14(9)", "15(10)"))

# Richness per treatment
richnessplot<-cbind(barplot$id, barplot$richness)

# Saving all the variables from the family analysis
meansfam<-means
installelevenfam<-installeleven
normalizedfam<-normalized
normalizedtfam<-normalizedt
successioncomparefam<-successioncompare
grazercomparefam<-grazercompare
#installelevendiv is the installeleven dataframe with Shannon diversity index ($plots), and a $zonetreat column for anovas

# DIVERSITY INDICES!
#Make a subset of installeleven, with just the plot numbers and stuff
shannon<-installeleven[14:72]
plots<-diversity(shannon, index = "shannon") # Make a vector plots, containing the diversity indices for each 
summary(plots) # Summary statistics for the plots....
plot(plots, xlab = "plot number", ylab = "Shannon Diversity Index", main = "Shannon Diversity Index for each Plot Installed in 2011")

installelevendiv$zonetreat<-paste(installelevendiv$zone, installelevendiv$treatment, sep="") # Paste $zone and $treatment in
# one column for easier anova analysis
installelevendiv$zonetreat<-as.factor(installelevendiv$zonetreat) # Make those pasted values into factors so I can plot it
plot(installelevendiv$zonetreat, installelevendiv$plots)
# I normalized the % covers for installelevendiv, because the ANOVAs for treatments are currently NOT significant (only low is different)
installelevendiv$normfactor<-100/installelevendiv$total_cover
# then divide each % cover by the normalizing value for that plot....
installelevendiv[14:72]<-installelevendiv[14:72]*installelevendiv$normfactor

shannonnorm<-installelevendiv[,14:72]
shannorm<-diversity(shannonnorm, index = "shannon")
installelevendiv$shannon_norm<-shannorm

# Let's do an ANOVA by treatment of the Shannon diversity indices for every plot:
plotaov<-aov(installelevendiv$plots~installelevendiv$zonetreat)

# Not very significant findings for Shannon diversity values
# Let's try scaling it to be between 0 and 1 (don't think that will change the values though...)
#Let's get evenness, Pielou's evenness J=H'/log(s)
J<-H/log(specnumber(shannonnorm))
specnumber(shannonnorm)
log(specnumber(shannonnorm))
J<-shannorm/(log(specnumber(shannonnorm)))
installelevendiv$J<-J
test<aov(installelevendiv$J~installelevendiv$treatment)

# Now let's do the inverse Simpson diversity index:
invsimpson<-diversity(shannonnorm, "inv")
simpson<-diversity(shannonnorm, "simpson")
test<-aov(installelevendiv$invsimpson~installelevendiv$zonetreat) # Does richness differ among zones and treatments (only zones - damn)

# Let's see if richness varies differently among treatments:
plot(installelevendiv$order,installelevendiv$specnumber, xaxt="n", ylab="Species Richness per plot", main="Species richness per 0.25m^2 plot for each treatment by zone")
axis(1, at=c(1:9), c("lowCGA", "lowSGA", "lowSGR", "midCGA", "midSGA", "midSGR", "highCGA", "highSGA", "highSGR")) #plotting richness

test<-aov(installelevendiv$specnumber~installelevendiv$zonetreat) # Does richness differ among zones and treatments (only zones - damn)

family_richness<-specnumber(normalizedfam[,4:32])
# Plotting family richness
with(normalizedfam, plot(order,richness, xaxt="n", xlab = "zone and treatment (GA or GR grazers ambient \nor reduced, C/S control succession)", 
                         ylab = "family richness", main = "Family richness by treatment and zone"))
familybarplot$Hapalidiaceae<-c(0,0,0,0,0,0,6,1,2,6,0,3,0,0,0,2,2,1)

names<-c("Phaeophyta", "Rhodophyta", "Rhodophyta", "Rhodophyta", "Phaeophyta", "Chlorophyta","Chlorophyta","Rhodophyta","Rhodophyta",
  "Rhodophyta", "Rhodophyta","Phaeophyta","Rhodophyta","Rhodophyta","Chlorophyta","Phaeophyta","Phaeophyta","Rhodophyta",
  "Rhodophyta","Rhodophyta","Phaeophyta","Rhodophyta","Rhodophyta","Chlorophyta","Chlorophyta", "Rhodophyta","Rhodophyta", "Phaeophyta")

## TO MAKE THE TABLE YOU NEED FOR THE PHYLOGENY VALUES, USE BELOW TABLE
## (IF YOU NEED TO) NORMALIZE THE % COVERS, in the FAMILYFIGURE table, 

familybarplot<-familyfigure[,!(names(DF) %in% drops)] # previously made a vector of column names
# I wanted to drop
meanfamplot<-with(fambarplot, aggregate(fambarplot[,c(5:33)], by=list(Z=zone, Treat=treatment), mean))

# meanfamplot has the right treatment names
names<-meanfamplot[,1:2]
# FAMILY LEVEL BAR PLOT BY TREATMENT, WITH PROPORTIONS OF EACH DIVISION
p<-ggplot(famcountdummy, aes(x=order,y=family_totals, fill = Division))+geom_bar(position="stack")+labs(y="Family Richness", x="Zone and Treatment")
p+scale_fill_manual(values=c("#009900", "#663300", "#CC0000")) + theme(axis.text.x=element_text(size=12)) + theme(axis.text.y=element_text(size=12))+
  theme(legend.text = element_text(size = 14), legend.title=element_text(size=14))+ theme(axis.title.x = element_text(size=14), axis.title.y=element_text(size=14))
# END OF FAMILY LEVEL BAR PLOT STUFF

+geom_text(aes(x=x, y=y, label=names), size=4, colour="white")
x=c(1:11)
names<-c("C","SG+","SG-","","C","SG+","SG-","","C","SG+","SG-")
           y=c(9,9,9,0,14,17,19,0,17,15,13)
p+geom_text(aes(variable, `family_totals`, label = Treat), 
          position = position_dodge(width=1))
#Next steps - and the categories correctly.(do in GIMP)
# Prep a species-level plot like this as well - it will go way faster (use excel to sum the species/fam)
# Make a budget!
# Start working on 1) multivariate or 2) phylogeny star sunflower whatever representation. 
  # For star: Need normalized abundances (do I?) for family level, Reds only, by zone only. 