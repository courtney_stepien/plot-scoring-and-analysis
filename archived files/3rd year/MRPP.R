# MRPP STATISTICAL ANALYSIS - MULTI RESPONSE PERMUTATION PROCEDURE OF WITHIN VERSUS AMONG-GROUP DISSIMILARITIES

env<-c(fit$zonetreat)
envzone<-c(fit$zone)
mrpp(NMDS,env, distance="bray")
mrpp(NMDS, envzone, distance="bray")

low<-NMDS[c(1:18),]
mid<-NMDS[c(19:36),]
high<-NMDS[c(37:54),]

envlow<-c(fit[c(1:18),5])
envmid<-c(fit[c(19:36),5])
envhigh<-c(fit[c(37:54),5])

treat<-c(firstlowmv$treatment)
firstlowmvd<-firstlowmv[,2:50]
m<-mrpp(firstlowmvd, treat, distance="bray")
m


# WITHIN A ZONE STUDIES OF TREATMENT EFFECT:

mrpp(low, envlow, distance="bray")
mrpp(mid, envmid, distance="bray")
mrpp(high, envhigh, distance="bray")

# EXPLICITLY TEST TREATMENT BY TREATMENT IN EACH ZONE - CGA-SGA, CGA-SGR, SGA-SGR X LOW, MID AND HIGH

lowsga<-subset(installeleven, Zonetreat=="lowSGA")
lowsga<-lowsga[,c(14:72)]
lowsgr<-subset(installeleven, Zonetreat=="lowSGR")
lowsgr<-lowsgr[,c(14:72)]
lowcga<-subset(installeleven, Zonetreat=="lowCGA")
lowcga<-lowcga[,c(14:72)]

midsga<-subset(installeleven, Zonetreat=="midSGA")
midsga<-midsga[,c(14:72)]
midsgr<-subset(installeleven, Zonetreat=="midSGR")
midsgr<-midsgr[,c(14:72)]
midcga<-subset(installeleven, Zonetreat=="midCGA")
midcga<-midcga[,c(14:72)]

highsga<-subset(installeleven, Zonetreat=="highSGA")
highsga<-highsga[,c(14:72)]
highsgr<-subset(installeleven, Zonetreat=="highSGR")
highsgr<-highsgr[,c(14:72)]
highcga<-subset(installeleven, Zonetreat=="highCGA")
highcga<-highcga[,c(14:72)]

lowCGA_SGA<-rbind(lowsga,lowcga)
lowCGA_SGR<-rbind(lowcga,lowsgr)
lowSGA_SGR<-rbind(lowsgr,lowsga)

midCGA_SGA<-rbind(midcga,midsga)
midSGA_SGR<-rbind(midsga,midsgr)
midSGR_CGA<-rbind(midsgr,midcga)

highCGA_SGA<-rbind(highcga,highsga)
highCGA_SGR<-rbind(highcga,highsgr)
highSGA_SGR<-rbind(highsga,highsgr)

# IT'S ALL JUST PAIRWISE SETS, SO USE THE SAME VECTOR WHERE TREATMENT = 1 OR 2
envtreat<-c(rep(1,6), rep(2,6))

mrpp(lowCGA_SGA, envtreat, distance="bray")
mrpp(lowCGA_SGR, envtreat, distance="bray")
mrpp(lowSGA_SGR, envtreat, distance="bray")
mrpp(midCGA_SGA, envtreat, distance="bray")
mrpp(midSGA_SGR, envtreat, distance="bray")
mrpp(midSGR_CGA, envtreat, distance="bray")
mrpp(highCGA_SGA, envtreat, distance="bray")
mrpp(highCGA_SGR, envtreat, distance="bray")
mrpp(highSGA_SGR, envtreat, distance="bray")



# ALL OF THE ABOVE ANALYSIS, EXCEPT NOW AT THE FAMILY LEVEL TO SEE IF FAMILY DIFFERENCES MATTER - THIS COULD BE PROOF OF CONCEPT...
NMDSfamily<-fambarplot[,c(5:33)]
  library(MASS)
d<-dist(NMDSfamily)
fit<-isoMDS(d, k=2)
fit<-data.frame(fit)
fit$zone<-fambarplot$zone
fit$zonetreat<-paste(fambarplot$treatment, fambarplot$zone, sep="")
lowfit<-subset(fit, zone=="low") 
lx<-lowfit$points.1
ly<-lowfit$points.2
midfit<-subset(fit, zone=="mid")
mx<-midfit$points.1
my<-midfit$points.2
highfit<-subset(fit, zone=="high")
hx<-highfit$points.1
hy<-highfit$points.2

# MRPP ANALYSIS AT THE FMAILY LEVEL:

env<-c(as.factor(fit$zonetreat))
envzone<-c(fit$zone)
mrpp(NMDSfamily,env, distance="bray")
mrpp(NMDSfamily, envzone, distance="bray")

low<-NMDSfamily[c(1:18),]
mid<-NMDSfamily[c(19:36),]
high<-NMDSfamily[c(37:54),]

lowmid<-rbind(low, mid)
lowhigh<-rbind(low, high)
midhigh<-rbind(mid, high)

envtwozone<-c(rep(1,18), rep(2,18))

envlow<-c(fit[c(1:18),5])
envmid<-c(fit[c(19:36),5])
envhigh<-c(fit[c(37:54),5])

# WITHIN A ZONE STUDIES OF TREATMENT EFFECT:

mrpp(low, envlow, distance="bray")
mrpp(mid, envmid, distance="bray")
mrpp(high, envhigh, distance="bray")

# COMPARE TWO ZONES:

mrpp(lowmid, envtwozone, distance="bray")
mrpp(lowhigh, envtwozone, distance="bray")
mrpp(midhigh, envtwozone, distance="bray")

# EXPLICITLY TEST TREATMENT BY TREATMENT IN EACH ZONE - CGA-SGA, CGA-SGR, SGA-SGR X LOW, MID AND HIGH
fambarplotnorm$zonetreat<-paste(fambarplot$zone, fambarplot$treatment, sep="")
lowsga<-subset(fambarplotnorm, zonetreat=="lowSGA")
lowsga<-lowsga[,c(1:29)]
lowsgr<-subset(fambarplotnorm, zonetreat=="lowSGR")
lowsgr<-lowsgr[,c(1:29)]
lowcga<-subset(fambarplotnorm, zonetreat=="lowCGA")
lowcga<-lowcga[,c(1:29)]

midsga<-subset(fambarplotnorm, zonetreat=="midSGA")
midsga<-midsga[,c(1:29)]
midsgr<-subset(fambarplotnorm, zonetreat=="midSGR")
midsgr<-midsgr[,c(1:29)]
midcga<-subset(fambarplotnorm, zonetreat=="midCGA")
midcga<-midcga[,c(1:29)]

highsga<-subset(fambarplotnorm, zonetreat=="highSGA")
highsga<-highsga[,c(1:29)]
highsgr<-subset(fambarplotnorm, zonetreat=="highSGR")
highsgr<-highsgr[,c(1:29)]
highcga<-subset(fambarplotnorm, zonetreat=="highCGA")
highcga<-highcga[,c(1:29)]

lowCGA_SGA<-rbind(lowsga,lowcga)
lowCGA_SGR<-rbind(lowcga,lowsgr)
lowSGA_SGR<-rbind(lowsgr,lowsga)

midCGA_SGA<-rbind(midcga,midsga)
midSGA_SGR<-rbind(midsga,midsgr)
midSGR_CGA<-rbind(midsgr,midcga)

highCGA_SGA<-rbind(highcga,highsga)
highCGA_SGR<-rbind(highcga,highsgr)
highSGA_SGR<-rbind(highsga,highsgr)

# IT'S ALL JUST PAIRWISE SETS, SO USE THE SAME VECTOR WHERE TREATMENT = 1 OR 2
envtreat<-c(rep(1,6), rep(2,6))

mrpp(lowCGA_SGA, envtreat, distance="bray")
mrpp(lowCGA_SGR, envtreat, distance="bray")
mrpp(lowSGA_SGR, envtreat, distance="bray")
mrpp(midCGA_SGA, envtreat, distance="bray")
mrpp(midSGA_SGR, envtreat, distance="bray")
mrpp(midSGR_CGA, envtreat, distance="bray")
mrpp(highCGA_SGA, envtreat, distance="bray")
mrpp(highCGA_SGR, envtreat, distance="bray")
mrpp(highSGA_SGR, envtreat, distance="bray")

# PLOT DATA AT THE FAMILY LEVEL BY ZONE
plot(c(-75,90), c(-75,60), type="n", cex.lab = 1.25, cex.axis= 1.25, xlab= "Coordinate 1", ylab="Coordinate 2", main="Nonmetric MDS at the Family Level")
points(lx,ly, col="black", pch=16)
points(mx,my, col="red", pch=16)
points(hx,hy, col="blue", pch=16)
legend("topright", c("Low", "Mid", "High"), pch=16, col=c("black", "red", "blue"))


# MULTIVARIATE ANALYSIS OF PLOTS BY ZONE AND TREATMENT - NONMETRIC MULTIDIMENSIONAL SCALING
lowsgafit<-subset(fit, zonetreat=="lowSGA")
lowsgrfit<-subset(fit, zonetreat=="lowSGR")
lowcgafit<-subset(fit, zonetreat=="lowCGA")

midsgafit<-subset(fit, zonetreat=="midSGA")
midsgrfit<-subset(fit, zonetreat=="midSGR")
midcgafit<-subset(fit, zonetreat=="midCGA")

highsgafit<-subset(fit, zonetreat=="highSGA")
highsgrfit<-subset(fit, zonetreat=="highSGR")
highcgafit<-subset(fit, zonetreat=="highCGA")



# PLOTTNG AT THE FAMILY LEVEL BY ZONE AND TREATMENT
lsgax<-lowsgafit$points.1
lsgay<-lowsgafit$points.2
lsgrx<-lowsgrfit$points.1
lsgry<-lowsgrfit$points.2
lcgax<-lowcgafit$points.1
lcgay<-lowcgafit$points.2
msgax<-midsgafit$points.1
msgay<-midsgafit$points.2
msgrx<-midsgrfit$points.1
msgry<-midsgrfit$points.2
mcgax<-midcgafit$points.1
mcgay<-midcgafit$points.2
hsgrx<-highsgrfit$points.1
hsgry<-highsgrfit$points.2
hsgax<-highsgafit$points.1
hsgay<-highsgafit$points.2
hcgax<-highcgafit$points.1
hcgay<-highcgafit$points.2

plot(c(-170, 180), c(-170,120), type="n", cex.lab = 1.25, cex.axis= 1.25, xlab= "Coordinate 1", ylab="Coordinate 2", main="Nonmetric Multidimensional Scaling of Plots by Zone and Treatment")

points(lcgax, lcgay, col="black", pch=15, cex=1.25)
points(mcgax, mcgay, col="red", pch=15, cex=1.25)
points(hcgax, hcgay, col="blue", pch=15, cex=1.25)
points(lsgax, lsgay, col="black", pch=22, cex=1.25)
points(msgax, msgay, col="red", pch=22, cex=1.25)
points(hsgax, hsgay, col="blue", pch=22, cex=1.25)
points(lsgrx, lsgry, col="black", pch=7, cex=1.25)
points(msgrx, msgry, col="red", pch=7, cex=1.25)
points(hsgrx, hsgry, col="blue", pch=7, cex=1.25)

legend("topright", cex=.95, c("Low Control", "Low Succession Grazers +", "Low Succession Grazers -", "Mid Control", "Mid Succession Grazers +", "Mid Succession Grazers -", "High Control", "High Succession Grazers +", "High Succession Grazers -"), pch=c(15, 22, 7, 15, 22, 7, 15, 22, 7), col=c("black", "black", "black", "red", "red", "red", "blue", "blue", "blue"))
