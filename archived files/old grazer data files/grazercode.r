# a bunch of code for beginning to analyze the grazer counts
setwd("~/Documents/Algae/Data\ from\ Plots")
grazers<-read.csv("grazercountsaug.csv", header=T)
attach(grazers)
library(lattice)

#Let's first plot the data, to see how each treatment holds up 
#we'll plot it by treatment within each zone, so I'll make some other dataframes by zone
low<-subset(grazers, zone=="low")
mid<-subset(grazers, zone=="mid")
high<-subset(grazers, zone=="high")
low2011<-subset(low, install==2011)
mid2011<-subset(mid, install==2011)
highsub<-subset(high, ngrazers<1000)

#Now let's do a box and whiskers plot of the three treatments in each zone
with(highsub, plot(treat, ngrazers, xaxt="n", xlab="treatment, n = 6 per treatment", ylab="number of grazers", 
  main="Number of Grazers by Treatment in the High Zone"))
axis(1, at=c(1:3), c("intact, grazer+", "disturb, grazer+", "disturb, grazer-"))

#Now let's do a box and whiskers plot for the mid zone:
with(mid, plot(treat, ngrazers, xaxt="n", xlab="treatment, n = 9 per treatment", ylab="number of grazers", 
                   main="Number of Grazers by Treatment in the Mid Zone"))
axis(1, at=c(1:3), c("intact, grazer+", "disturb, grazer+", "disturb, grazer-"))

#also, let's do mid 2011, just the 6 reps that are on the same time frame
with(mid2011, plot(treat, ngrazers, xaxt="n", xlab="treatment, n = 6 per treatment", ylab="number of grazers", 
               main="Number of Grazers by Treatment in the Mid Zone"))
axis(1, at=c(1:3), c("intact, grazer+", "disturb, grazer+", "disturb, grazer-"))

#next, we'll do the same two plots for the low zone
with(low2011, plot(treat, ngrazers, xaxt="n", xlab="treatment, n = 6 per treatment", ylab="number of grazers", 
                   main="Number of Grazers by Treatment in the low Zone"))
axis(1, at=c(1:3), c("intact, grazer+", "disturb, grazer+", "disturb, grazer-"))

with(low, plot(treat, ngrazers, xaxt="n", xlab="treatment, n = 9 per treatment", ylab="number of grazers", 
                   main="Number of Grazers by Treatment in the Low Zone"))
axis(1, at=c(1:3), c("intact, grazer+", "disturb, grazer+", "disturb, grazer-"))

#Let's do some statistics on the low zone, to see if the number of grazers is significantly less in the reduction treatments
summary(with(low, aov(ngrazers~treat)))
summary(with(low, aov(ngrazers~treatgrazer)))
summary(with(low, aov(ngrazers~treatgrazer*treatdisturbance)))

#Low zone of plots just installed in 2011
summary(with(low2011, aov(ngrazers~treat)))
summary(with(low2011, aov(ngrazers~treatgrazer)))

#Mid zone
summary(with(mid, aov(ngrazers~treat)))
summary(with(mid, aov(ngrazers~treatgrazer)))

#mid zone 2011 only
summary(with(mid2011, aov(ngrazers~treat)))
summary(with(mid2011, aov(ngrazers~treatgrazer)))
model<-lm(with(mid2011, ngrazers~treatgrazer*treatdisturbance))
summary(with(mid2011, aov(ngrazers~treatdisturbance*treatgrazer)))
summary(aov(model))

#Test to troubleshoot model to get interaction - we do can interaction effects on the whole data set, but not the individual zones...
test<-lm(with(mid, ngrazers~install))

#high zone (no new plots installed in 2011, so the whole dataset is 2011)
summary(with(high, aov(ngrazers~treat)))
summary(with(high, aov(ngrazers~treatgrazer)))

#get mean values for each zone
means<-aggregate(ngrazers~zone,grazers,mean)
means.sd<-aggregate(ngrazers~zone, grazers,sd)
means$sd<-means.sd
nperzone<-aggregate(ngrazers~zone, grazers, length)
means$n<-nperzone
means$ste<-with(means,sd/sqrt(n))

#plot all data as points, maybe color by treatment?
plot(c(1,3), c(0,1100), type = "n", xaxt = "n", xlab = "Zone", ylab = "number of grazers")