# NMDS

# Data input requirements:
    # Species names as column headers
    # Columns contain species only - no other information about the plot for the actual analysis file
        # Can subset input file for the zone and treatments of interest to keep the data together
        # Can add plot identifier in row name

# BASIC INPUT AND DATAFRAME SETUP

setwd("~/Documents/Algae/Data\ from\ Plots/2013/input")
library(vegan)
library(MASS)
census<-read.csv("FieldData2013_02_14input4.csv", header=TRUE) #if you want to give the rows names, use row.names=column number containing the names
census[is.na(census)]<-0 #replace all na's with 0's
census2011<-subset(census, installyear==2011)
census2011<-subset(census2011, Interval!=1)
census2011$plotbyint<-paste(census2011$plot_name, census2011$Interval, sep = "_") # Gives each row a unique name, combination plot code and interval
rownames(census2011)<-census2011$plotbyint


# OPTIONS FOR WORKING WITH THE ALGAL PERCENT COVER DATA:
    # Make a normalizing constant so we are analyzing algal community composition to multiply all the column percent covers against: 100%/total algal cover
        # Normalizing 1, 'normbyalgae': by total algal cover (100%/algal cover, multiply this value by each % cover)
        # Normalizing 2, 'normbytotal': by total cover (100%/total cover, multiply this value by each % cover)
    # Just use the raw percent covers
    
    # Normalizing option 1, norm by total algal cover
    normbyalgae<-100/census2011$algal_cover
    normbyalgae[is.infinite(normbyalgae)]<-1
        # The above cleans the vector of Infinity values, sets them to 1 - some of the plots had 0% algal cover, so 100/0 = infinity
    normalgae2011<-census2011[,c(25:73)]*normbyalgae

    # Normalizing option 2, norm by total percent cover (includes non-algal organisms, bare rock)
    normbytotal<-100/census2011$total_cover
    normtotal2011<-census2011[,c(25:73)]*normbytotal

    # Raw percent covers - just subset census2011 file
    rawcover2011<-census2011[,c(25:73)]

# First, NMDS on entire dataset at once

normtotal2011.mds<-metaMDS(normtotal2011, trace = FALSE)
plot(normtotal2011.mds, type = "t") # Makes a plot with text - plot names and species names
plot(normtotal2011.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from census2011

nt<-data.frame(scores(normtotal2011.mds))
nt$month<-census2011$monthsinceinstall
nt$zone<-census2011$zone
nt$treatment<-census2011$treatment
nt$zonetreat<-paste(nt$zone, nt$treat, sep="")
write.csv(nt, file = "NMDSscores.csv")

# Next steps: what do I want to plot to demonstrate?
  # For plot with everything, add low, mid and high coloration

library(ggplot2)
ggplot(nt, aes(x=NMDS1, y=NMDS2, colour=zone)) + geom_point(size=3) + 
  scale_color_manual(values=c("#56B4E9", "#CC79A7", "#000000")) + 
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=15)) +
  ggtitle("NMDS of plot composition by zone (richness and percent cover)\nall treatments contained within zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=15), axis.title.y = element_text(size=20),
  axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))
  
  # For plot with all data points and all times, by zone-treatment
ggplot(nt, aes(x=NMDS1, y=NMDS2, colour=zonetreat)) + geom_point(size=3) +
  scale_color_manual(values=c("#000000", "#993300", "#FF9900", "#0000FF", "#00FF00", "#66FFFF", "#FF0033", "#660066", "#999999")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=15)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby zone and treatment ") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=15), axis.title.y = element_text(size=20),
  axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

  # Plot all data points all times, facet by zone, differentiate between three colors for treatments

ggplot(nt, aes(x=NMDS1, y=NMDS2, colour=treatment)) + geom_point(size=3) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  facet_wrap(~zone) + theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=15)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby zone and treatment, subset by zone ") + 
  theme(plot.title = element_text(size=20)) + theme(strip.text.x=element_text(size=20)) +
  theme(legend.text=element_text(size=15), axis.title.y = element_text(size=20),
  axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

  # All data points at all times, color by treatment, no facet

ggplot(nt, aes(x=NMDS1, y=NMDS2, colour=treatment)) + geom_point(size=3) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) + 
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=15)) +
  ggtitle("NMDS of plot composition by treatment (richness and percent cover)") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=15), axis.title.y = element_text(size=20),
  axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

  # Subset the data by controls, color by zone
ntcontrol<-subset(nt, treatment=="CGA")

ggplot(ntcontrol, aes(x=NMDS1, y=NMDS2, colour=zone)) + geom_point(size=3) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) + 
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of control plot composition by zone\n(richness and percent cover)") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

# Next, NMDS using subset of data - the intervals that you care about
    # In LibreCalc, I deleted the score data from intervals I didn't want to compare, then read the csv back into R as"ntintsubset"
setwd("~/Documents/Algae/Data\ from\ Plots/2013/input/NMDS")
ntintsubset<-read.csv("NMDSintervalsubset.csv")
ntintlow<-subset(ntintsubset, zone=="low")
ntintmid<-subset(ntintsubset, zone=="mid")
ntinthigh<-subset(ntintsubset, zone=="high")

ggplot(ntintlow, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the low zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(ntintmid, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the mid zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(ntinthigh, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the high zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

# TIME TO SUBSET INPUT DATA FILE AND RUN 3 NMDS ANALYSES, 1 for each zone
setwd("~/Documents/Algae/Data\ from\ Plots/2013/input/NMDS")
write.csv(census2011, file="census2011intervalsubset.csv")
  # Opened the file in LibreCalc, deleted all months but 1,2,12,14, then imported back into R
intsubset<-read.csv("census2011intervalsubset.csv", header=TRUE, row.names=1)

    # Normalizing option 1, norm by total algal cover
    intnormbyalgae<-100/intsubset$algal_cover
    intnormbyalgae[is.infinite(intnormbyalgae)]<-1
    # The above cleans the vector of Infinity values, sets them to 1 - some of the plots had 0% algal cover, so 100/0 = infinity
    intnormalgae2011<-intsubset[,c(25:73)]*intnormbyalgae
    intnormalgae2011$zone<-intsubset$zone
    intnormalgae2011$treatment<-intsubset$treatment
    intnormalgae2011$month<-intsubset$monthsinceinstall

    # Normalizing option 2, norm by total percent cover (includes non-algal organisms, bare rock)
    intnormbytotal<-100/intsubset$total_cover
    intnormtotal2011<-intsubset[,c(25:73)]*intnormbytotal
    intnormtotal2011$zone<-intsubset$zone
    intnormtotal2011$treatment<-intsubset$treatment
    intnormtotal2011$month<-intsubset$monthsinceinstall

    # Raw percent covers - just subset census2011 file
    intrawcover2011<-intsubset[,c(25:73)]

# NOW split each normalized dataset into low, mid and high zone subsets for NMDS analysis
    # Norm by algae:
intsubalglow<-subset(intnormalgae2011, zone=="low")
intsubalglow<-intsubalglow[,c(1:49)]
intsubalgmid<-subset(intnormalgae2011, zone=="mid")
intsubalgmid<-intsubalgmid[,c(1:49)]
intsubalghigh<-subset(intnormalgae2011, zone=="high")
intsubalghigh<-intsubalghigh[,c(1:49)]

    # Norm by total cover:
intsubntlow<-subset(intnormtotal2011, zone=="low")
intsubntlow<-intsubntlow[,c(1:49)]
intsubntmid<-subset(intnormtotal2011, zone=="mid")
intsubntmid<-intsubntmid[,c(1:49)]
intsubnthigh<-subset(intnormtotal2011, zone=="high")
intsubnthigh<-intsubnthigh[,c(1:49)]

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to total cover: LOW
intsubntlow.mds<-metaMDS(intsubntlow, trace = FALSE)
plot(intsubntlow.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubntlow.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from census2011
# For low zone interval NMDS: 
lowintnt<-data.frame(scores(intsubntlow.mds))
intsubntlow<-subset(intnormtotal2011, zone=="low")
lowintnt$month<-intsubntlow$month
lowintnt$zone<-intsubntlow$zone
lowintnt$treatment<-intsubntlow$treatment
lowintnt$zonetreat<-paste(lowintnt$zone, lowintnt$treat, sep="")
write.csv(lowintnt, file = "NMDSscoresLOWINT.csv")

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to total cover: MID
intsubntmid.mds<-metaMDS(intsubntmid, trace = FALSE)
plot(intsubntmid.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubntmid.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from census2011
# For low zone interval NMDS: 
midintnt<-data.frame(scores(intsubntmid.mds))
intsubntmid<-subset(intnormtotal2011, zone=="mid")
midintnt$month<-intsubntmid$month
midintnt$zone<-intsubntmid$zone
midintnt$treatment<-intsubntmid$treatment
midintnt$zonetreat<-paste(midintnt$zone, midintnt$treat, sep="")
write.csv(midintnt, file = "NMDS scores MID interval.csv")

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to total cover: HIGH
intsubnthigh.mds<-metaMDS(intsubnthigh, trace = FALSE)
plot(intsubnthigh.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubnthigh.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from census2011
# For low zone interval NMDS: 
highintnt<-data.frame(scores(intsubnthigh.mds))
intsubnthigh<-subset(intnormtotal2011, zone=="high")
highintnt$month<-intsubnthigh$month
highintnt$zone<-intsubnthigh$zone
highintnt$treatment<-intsubnthigh$treatment
highintnt$zonetreat<-paste(highintnt$zone, highintnt$treat, sep="")
write.csv(highintnt, file = "NMDS scores HIGH interval.csv")

# Plots of subset NMDS by zone:
ggplot(lowintnt, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the low zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(midintnt, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the mid zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(highintnt, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the high zone") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to algal cover: LOW
intsubnalglow.mds<-metaMDS(intsubalglow, trace = FALSE)
plot(intsubalglow.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubalglow.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from intnormtotal2011
# For low zone interval NMDS: 
lowintnalg<-data.frame(scores(intsubnalglow.mds))
intsubnalglow<-subset(intnormalgae2011, zone=="low")
lowintnalg$month<-intsubnalglow$month
lowintnalg$zone<-intsubnalglow$zone
lowintnalg$treatment<-intsubnalglow$treatment
lowintnalg$zonetreat<-paste(lowintnalg$zone, lowintnalg$treat, sep="")
write.csv(lowintnalg, file = "NMDS scores LOW INT norm algae.csv")

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to algal cover: MID
intsubnalgmid.mds<-metaMDS(intsubalgmid, trace = FALSE)
plot(intsubnalgmid.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubnalgmid.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from intnormtotal2011
# For low zone interval NMDS: 
midintnalg<-data.frame(scores(intsubnalgmid.mds))
intsubnalgmid<-subset(intnormalgae2011, zone=="mid")
midintnalg$month<-intsubnalgmid$month
midintnalg$zone<-intsubnalgmid$zone
midintnalg$treatment<-intsubnalgmid$treatment
midintnalg$zonetreat<-paste(midintnalg$zone, midintnalg$treat, sep="")
write.csv(midintnalg, file = "NMDS scores MID interval norm algae.csv")

# NMDS USING SUBSET OF INTERVAL DATA BY ZONE, normalized to algal cover: HIGH
intsubnalghigh.mds<-metaMDS(intsubalghigh, trace = FALSE)
plot(intsubnalghigh.mds, type = "t") # Makes a plot with text - plot names and species names
plot(intsubnalghigh.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from intnormtotal2011
# For low zone interval NMDS: 
highintnalg<-data.frame(scores(intsubnalghigh.mds))
intsubnalghigh<-subset(intnormtotal2011, zone=="high")
highintnalg$month<-intsubnalghigh$month
highintnalg$zone<-intsubnalghigh$zone
highintnalg$treatment<-intsubnalghigh$treatment
highintnalg$zonetreat<-paste(highintnalg$zone, highintnalg$treat, sep="")
write.csv(highintnalg, file = "NMDS scores HIGH interval norm algae.csv")

# Plots of subset NMDS by zone:
ggplot(lowintnalg, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the low zone, normalized to % algal cover") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(midintnalg, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the mid zone, normalized to % algal cover") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

ggplot(highintnalg, aes(x=NMDS1, y=NMDS2, colour=treatment, label=month)) + geom_text(size=5) + 
  scale_color_manual(values=c("#000000", "#0000FF", "#FF0033")) +
  theme(strip.text.x=element_text(size=25)) + theme(legend.title=element_text(size=20)) +
  ggtitle("NMDS of plot composition (richness and percent cover)\nby treatment in the high zone, normalized to % algal cover") + 
  theme(plot.title = element_text(size=20)) +
  theme(legend.text=element_text(size=20), axis.title.y = element_text(size=20),
        axis.title.x = element_text(size=20), axis.text.x = element_text(size=20), axis.text.y=element_text(size=20))

