# NMDS

# Data input requirements:
    # Species names as column headers
    # Columns contain species only - no other information about the plot for the actual analysis file
        # Can subset input file for the zone and treatments of interest to keep the data together
        # Can add plot identifier in row name

# BASIC INPUT AND DATAFRAME SETUP

setwd("~/Documents/Algae/Data\ from\ Plots/2013/input")
library(vegan)
library(MASS)
census<-read.csv("FieldData2013_02_14input4.csv", header=TRUE) #if you want to give the rows names, use row.names=column number containing the names
census[is.na(census)]<-0 #replace all na's with 0's
census2011<-subset(census, installyear==2011)
census2011<-subset(census2011, Interval!=1)
census2011$plotbyint<-paste(census2011$plot_name, census2011$Interval, sep = "_") # Gives each row a unique name, combination plot code and interval
rownames(census2011)<-census2011$plotbyint


# OPTIONS FOR WORKING WITH THE ALGAL PERCENT COVER DATA:
    # Make a normalizing constant so we are analyzing algal community composition to multiply all the column percent covers against: 100%/total algal cover
        # Normalizing 1, 'normbyalgae': by total algal cover (100%/algal cover, multiply this value by each % cover)
        # Normalizing 2, 'normbytotal': by total cover (100%/total cover, multiply this value by each % cover)
    # Just use the raw percent covers
    
    # Normalizing option 1, norm by total algal cover
    normbyalgae<-100/census2011$algal_cover
    normbyalgae[is.infinite(normbyalgae)]<-1
        # The above cleans the vector of Infinity values, sets them to 1 - some of the plots had 0% algal cover, so 100/0 = infinity
    normalgae2011<-census2011[,c(25:73)]*normbyalgae

    # Normalizing option 2, norm by total percent cover (includes non-algal organisms, bare rock)
    normbytotal<-100/census2011$total_cover
    normtotal2011<-census2011[,c(25:73)]*normbytotal

    # Raw percent covers - just subset census2011 file
    rawcover2011<-census2011[,c(25:73)]

# First, NMDS on entire dataset at once

normtotal2011.mds<-metaMDS(normtotal2011, trace = FALSE)
plot(normtotal2011.mds, type = "t") # Makes a plot with text - plot names and species names
plot(normtotal2011.mds, type = "p") # Just has points

# To pull out actual scores from NMDS and add signifiers - add zone, treatment, zonetreat, interval columns, from census2011

nt<-data.frame(scores(normtotal2011.mds))
nt$month<-census2011$monthsinceinstall
nt$zone<-census2011$zone
nt$treatment<-census2011$treatment
write.csv(nt, file = "NMDSscores.csv")

# Next steps: what do I want to plot to demonstrate?
  # For plot with everything, add low, mid and high coloration
  # Add zone, treatment, zonetreat, interval columns to point data to be able to subset into different datasets to plot
  # By interval
      # Plot 1st after installation, August 2011, June 2012, August 2012
  # By zone and treatment
      # Just put low, mid, high control plots to compare
      # Plot interval of four dates for low zone treatments 
          # One plot for SGA
          # One plot for SGR

