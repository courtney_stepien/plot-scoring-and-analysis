# Fake Data analysis

fakedata<-read.csv("fakedata.csv")

# ANOVA attempt 1a. 
# Question: what are the effects of zone and treatment on species number? time and transect as random effects
# ERROR input as TRANSECT being the largest source of error
anova1a<-aov(total_species~zone*treatment + Error(Interval), data=fakedata)
summary(anova1a)

# ANOVA attempt 1b.
# Question: what are the effects of zone and treatment on species number? time and transect as random effects
# ERROR input as TIME being the largest source of error
anova1b<-aov(total_species~zone*treatment + Error(Interval), data=fakedata)
summary(anova1b)

# LME attempt 2a.
# Questions: what are the effects of zone and treatment on species number? time and transect as random effects
# Data type = Poisson, because the data are species counts
lmer2a<-lmer(total_species~zone*treatment + (1|Interval), family="poisson", data=fakedata)
summary(lmer2a)

# LME attempt 2b.
# Questions: what are the effects of zone and treatment on species number? Just looking at transect for random effect, no time
# Data type = Poisson, because the data are species counts
lmer2b<-lmer(total_species~zone*treatment + (1|transect), family="poisson", data=fakedata)
summary(lmer2b)

# LME attempt 2c.
# Questions: what are the effects of zone and treatment on species number? Just looking at time for random effect, no transect
# Data type = Poisson, because the data are species counts
lmer2c<-lmer(total_species~treatment + (1|Interval), family="poisson", data=fakedata)
summary(lmer2c)

# LME attempt 3a.
# Questions: what are the effects of zone and treatment on species number? time and transect as 
# random (nested??) effects
# Data type = Poisson, because the data are species counts
lmer3a<-lmer(total_species~zone*treatment + (Interval|transect), family="poisson", data=fakedata)
summary(lmer3a)

# LME attempt 3b.
# Questions: what are the effects of zone and treatment on species number? transect and time as nested random effects???
# Data type = Poisson, because the data are species counts
lmer3b<-lmer(total_species~zone*treatment + (transect|Interval), family="poisson", data=fakedata)
summary(lmer3b)

# LME attempt 4a.
# Questions: what are the effects of zone, Interval and treatment on species number? 
# Transect as random effect, time as fixed effect
# Data type = Poisson, because the data are species counts
lmer4a<-lme(total_species~zone*treatment*Interval, random = ~1|transect, data=fakedata)
summary(lmer4a)