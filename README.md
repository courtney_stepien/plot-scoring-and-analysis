# Experimental plot scoring and analysis

## Intro

This project is a 4-year long investigation of ecological community responses to disturbance. 

My goal is to understand how seaweed communities change as they recover from a press disturbance. This repo is to manage the data entry and analysis as I score photos of seaweed communities for different species' presence and abundance. 

## License

The content of this project itself is licensed under the [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by/4.0/), and the code used to analyze and visualize data is licensed under the [MIT license](https://opensource.org/licenses/mit-license.php).
